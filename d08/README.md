# D08

# Reusable App
Le but est de reutiliser le code qui a deja ete ecrit dans des application 
et d'exporter les applications afin d'en faire un package installable avec pip.
Dans le settings.py on peut ajouter des appliacations en tant que paauit pip.
La construction du package necessite un install.py
pyhton3 setup.py # cree un paquet (archive) dans ./dist
pip install myapp.tar.gz

# Nginx simple
Rendre accessible nos pages depuis un serveur http.
website.conf 
server{
	listen 8001;
	location /django/ {
			autoindex on;
			alias /nfs/2016/v/vmonteco/...
		}
}
Le code python n'est pas interprete.

# Assets
Les assets sont des fichiers a servir tel quel (css, javascript, etc.)
Il y a deux categories d'assets, media files et static files.
Les statics files sont des fichiers du projet, javscript, image de fond, css...
Les medias files peuvent etre des fichiers ammovible, non necesaires au projet,
photo uplodees par le user, etc...

# STATIC Files
settings.py
STATIC_URL = '/static/'
STATIC_ROOT = os.path.join(BASE_DIR, 'staticfiles')
On met les fichiers static de l'application dans un dossier appname/static/appname
urls.py
if settings.DEBUG:
	urlpatterns +=static(settings.STATIC_URL, document_root=settings.STATIC_ROOT)
./manage.py collectstatic

# STATIC Files
settings.py
MEDIA_URL = '/media/'
MEDIA_ROOT = os.path.join(BASE_DIR, 'mediafiles')

# WSGI
La Web Server Gateway Interface (WSGI) est une spécification qui définit une 
interface entre des serveurs et des applications web pour le langage Python.

# Nginx + uWSGI
Utilitaire qui permet de lancer un serveur WSGI pour servir le projet django.
pip install uWSGI
1 lancer uWSGI
2 lancer Nginx

# Nginx + Gunicorn
Utilitaire qui permet de lancer un serveur WSGI pour servir le projet django.
1 lancer Gunicorn
2 lancer Nginx

# HTTPS
Creer un certificat et une cle.

# Websockets
Protocole ASGI pour utiliser les websocket.
Le server a utiliser est daphne.












