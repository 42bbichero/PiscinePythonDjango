#!/bin/sh

# Create virtualenv
echo "# Create virtualenv"
echo "virtualenv --python=/Users/bbichero/.brew/bin/python3.7 ."
virtualenv --python=/Users/bbichero/.brew/bin/python3.7 .

# source env
echo
echo "# source env"
echo "source bin/activate"
#source bin/activate
source /Users/bbichero/d08/bin/activate

# install pip requirements
echo
echo "# install pip requirements"
echo "pip install -r requirement.txt"
pip install -r requirement.txt

# install ex00 app
echo
echo "# install ex00 app"
echo "pip install ex00-0.1.tar.gz"
pip install ex00-0.1.tar.gz

# Migrate
echo
echo "# Migrate"
echo "python manage.py makemigrations zero"
python manage.py makemigrations ex00 zero
echo "python manage.py migrate"
python manage.py migrate
echo "python manage.py migrate --run-syncdb"
python manage.py migrate --run-syncdb

# collectstatic
echo "# collectstatic"
echo "python manage.py collectstatic"
python manage.py collectstatic

# Copy cert file
echo
echo "# Copy cert file"
echo "cp nginx/localhost.* ~/.brew/etc/nginx/"
cp nginx/localhost.* ~/.brew/etc/nginx/

# Copy nginx d08 conf
echo
echo "# Copy nginx d08 conf"
echo "cp nginx/d08.conf ~/.brew/etc/nginx/servers/"
cp nginx/d08.conf ~/.brew/etc/nginx/servers/

# restart nginx
echo
echo "# restart nginx"
echo "brew services restart nginx"
brew services restart nginx

# kill all uwsgi session, and restart it
echo
echo "# kill all uwsgi session, and restart it"
echo "killall uwsgi"
killall uwsgi
echo "uwsgi --ini uwsgi.ini"
uwsgi --ini uwsgi.ini

tail -f /tmp/uwsgi_daemonize.log /Users/bbichero/logs/*
