from django.db import models

# Create your models here.
class Image(models.Model):
    title = models.CharField(null=False, max_length=10)
    image = models.FileField(null=False)
