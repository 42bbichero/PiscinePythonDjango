from django.shortcuts import render
from django.http import HttpResponseRedirect

from .models import Image
from .forms import imageForm

def init(request):
    if request.method == 'POST':
        form = imageForm(request.POST, request.FILES)
        count = Image.objects.filter(image=request.FILES['image']).count()
        if count > 0:
            return HttpResponseRedirect('?message=image already uploaded')
        if form.is_valid():
            form.save()
            return HttpResponseRedirect('?message=success')
    else:
        form = imageForm().as_p()

    message = ""
    if 'message' in request.GET:
        message = request.GET.get('message')

    images = Image.objects.all()

    return render(request, 'ex00/main.html', { 'form': form, 'images': images, 'message': message })
