from django import forms
from .models import Image

class imageForm(forms.ModelForm):
    class Meta:
        model = Image
        fields = ['title', 'image']
