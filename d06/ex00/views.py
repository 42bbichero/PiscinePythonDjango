from django.shortcuts import render
from django.conf import settings
from random import randint

# Create your views here.
def init(request):
    if request.session and request.session.get('username'):
        username = request.session['username']
    else:
        username = settings.USERNAMES[randint(0, 9)]
        request.session['username'] = username

    context = {
            'username': username,
            'cookie': request.session
    }
    return render(request, 'ex00/main.html', context)
