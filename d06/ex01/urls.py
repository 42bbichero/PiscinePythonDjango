from django.conf.urls import url, include
from django.views.generic import TemplateView
from django.contrib.auth.decorators import login_required
from . import views

urlpatterns = [
    url(r'^$', views.init, name='ex01'),
    url(r'home$', views.init, name='ex01'),
    url(r'login$', views.login, name='ex01'),
    url(r'signup$', views.signup, name='ex01'),
    url(r'logout$', TemplateView.as_view(template_name='ex01/logout.html'))
]
