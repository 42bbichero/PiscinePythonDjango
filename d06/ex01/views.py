from django.shortcuts import render
from django.conf import settings
from django.contrib.auth.decorators import login_required
from .models import User
from .forms import *

# Create your views here.
def init(request):
    #if request.session and request.session.get('username'):
    #    username = request.session['username']
    #else:
    #    request.session['username'] = username

    #context = {
    #        'username': username,
    #        'cookie': request.session
    #}
    return render(request, 'ex01/main.html')

def signup(request):
    context = {}
    url = 'ex01/signup.html'

    if request.method == 'POST':
        form = signupForm(request.POST)
        if form.is_valid():
            form.save()
            url = 'ex01/login.html'
            context['form'] = loginForm().as_p()
        else:
            context['form'] = form.as_p()
    else:
        context['form'] = signupForm().as_p()

    return render(request, url, context)

def login(request):
    url = 'ex01/login.html'
    context = {}
    context['form'] = loginForm().as_p()

    if request.session and request.session.get('username'):
        username = request.session['username']
        user_db = User.objects.filter(username=username)
        if user_db.count():
            return redirect('/ex01/main')

    if request.method == 'POST':
        form = loginForm(request.POST)
        if form.is_valid():
            form.save()
            request.session['username'] = request.POST['username']
            url = 'ex01/main.html'
        else:
            context['form'] = form.as_p()

    return render(request, url, context)

@login_required
def checkLogin(request):
    return render(request, 'ex01/logout.html')
