from django.contrib.auth.models import PermissionsMixin
from django.contrib.auth.base_user import AbstractBaseUser
from django.db import models

class User(AbstractBaseUser, PermissionsMixin):
    class Meta:
        app_label = 'ex01'

    id = models.AutoField(primary_key=True)
    update_date = models.DateTimeField(auto_now=True)
    creation_date = models.DateTimeField(auto_now_add=True)
    username = models.CharField(
        max_length=150,
        unique=True
    )

    USERNAME_FIELD = 'username'
    REQUIRED_FIELDS = []

    def __str__(self):
        return self.username
