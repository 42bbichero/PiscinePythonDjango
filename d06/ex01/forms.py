from django import forms
from django.contrib import auth
from .models import User

class signupForm(forms.ModelForm):
    username = forms.CharField(label='username', widget=forms.TextInput, required=True, min_length=8)
    password = forms.CharField(label='Password', widget=forms.PasswordInput, required=True, min_length=8)
    password_retry = forms.CharField(label='Password confirmation', widget=forms.PasswordInput, required=True, min_length=8)

    class Meta:
        model = User
        fields = ['username', 'password']

    def clean_username(self):
        username = self.cleaned_data['username'].lower()
        r = User.objects.filter(username=username)

        if r.count():
            raise forms.ValidationError("Username already exists")
        return username

    def clean_password_retry(self):
        # Check that the two password entries match
        password = self.cleaned_data.get("password")
        password_retry = self.cleaned_data.get("password_retry")

        if password and password_retry and password != password_retry:
            raise forms.ValidationError("Passwords don't match")
        return password_retry

    def save(self, commit=True):
        # Save the provided password in hashed format
        user = super(signupForm, self).save(commit=False)
        user.set_password(self.cleaned_data["password"])

        if commit:
            user.save()
        return user

class loginForm(forms.ModelForm):
    username = forms.CharField(label='username', widget=forms.TextInput, required=True, min_length=8)
    #username = forms.CharField(
    #        _('username'),
    #        max_length=150,
    #        unique=True,
    #        help_text=_('Required. 150 characters or fewer. Letters, digits and @/./+/-/_ only.'),
    #        validators=[username_validator],
    #        error_messages={
    #            'unique': _("A user with that username already exists."),
    #            },
    #        )
    password = forms.CharField(label='Password', widget=forms.PasswordInput, required=True, min_length=8)

    class Meta:
        model = User
        fields = ['username', 'password']

    def clean_username(self):
        username = self.cleaned_data['username'].lower()
        r = User.objects.filter(username=username)

        if not r.count():
            raise forms.ValidationError("Unknow username")
        return username

    def clean_password(self):
        # Check that the two password entries match
        username = self.cleaned_data.get("username")
        password = self.cleaned_data.get("password")
        user = User.objects.filter(username=username, password=password)

        if not user.count():
            raise forms.ValidationError("Invalid password")
        return password

    def save(self, commit=True):
        # create and save cookie in db
        # Save the provided password in hashed format
        return redirect('/ex01/home')
