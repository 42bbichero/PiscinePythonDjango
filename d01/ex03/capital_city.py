import sys

def find_capital(states, capital_cities, state):
    short_state = states.get(state)

    if not short_state:
        print("Unknown state")
        exit()

    captial = capital_cities.get(short_state)
    print(captial) 

def handle_arg():
    states = {
            "Oregon"    : "OR",
            "Alabama"   : "AL",
            "New Jersey": "NJ",
            "Colorado"  : "CO"
    }
    
    capital_cities = {
            "OR": "Salem",
            "AL": "Montgomery",
            "NJ": "Trenton",
            "CO": "Denver"
    }

    if len(sys.argv) != 2:
        exit()

    find_capital(states, capital_cities, sys.argv[1])

if __name__ == '__main__':
    handle_arg()
