def my_var():
    qwe = 42;
    qwe1 = "42";
    qwe2 = "quarante-deux";
    qwe3 = 42.0;
    qwe4 = True;
    qwe5 = [42];
    qwe6 = {42: 42};
    qwe7 = (42,);
    qwe8 = set();

    print(qwe, "est de type", type(qwe)); 
    print(qwe1, "est de type", type(qwe1)); 
    print(qwe2, "est de type", type(qwe2)); 
    print(qwe3, "est de type", type(qwe3)); 
    print(qwe4, "est de type", type(qwe4)); 
    print(qwe5, "est de type", type(qwe5)); 
    print(qwe6, "est de type", type(qwe6)); 
    print(qwe7, "est de type", type(qwe7)); 
    print(qwe8, "est de type", type(qwe8)); 

if __name__ == '__main__':
    my_var();
