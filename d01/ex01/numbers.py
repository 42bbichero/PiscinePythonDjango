def print_numbers():
    try:
        with open("numbers.txt") as f:
            for line in f:
                for number in line.split(","):
                    print(number.strip())  
    except:
        exit()

if __name__ == '__main__':
    print_numbers()
