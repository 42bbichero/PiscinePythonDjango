import sys

def find_key_by_val(val, dic):
    for key, value in dic.items():
        if value == val:
            return key
    return None

def find_state(states, capital_cities, capital):
    short_cap = find_key_by_val(capital, capital_cities)

    if not short_cap:
        print("Unknown capital city")
        exit()

    state = find_key_by_val(short_cap, states)
    print(state) 

def handle_arg():
    states = {
            "Oregon"    : "OR",
            "Alabama"   : "AL",
            "New Jersey": "NJ",
            "Colorado"  : "CO"
    }
    
    capital_cities = {
            "OR": "Salem",
            "AL": "Montgomery",
            "NJ": "Trenton",
            "CO": "Denver"
    }

    if len(sys.argv) != 2:
        exit()

    find_state(states, capital_cities, sys.argv[1])

if __name__ == '__main__':
    handle_arg()
