import sys

def find_val_by_key(cle, dic):
    for key, value in dic.items():
        if cle.lower() == key.lower():
            return value
    return None

def find_key_by_val(val, dic):
    for key, value in dic.items():
        if value.lower() == val.lower():
            return key
    return None

def find_capital(state, short_state, capital_cities):
    capital = find_val_by_key(short_state, capital_cities)
    print(capital, "is the capital of", state) 

def find_state(capital, short_cap, states):
    state = find_key_by_val(short_cap, states)
    print(capital, "is the capital of", state) 

def parse_array(states, capital_cities, array):
    for elem in array:
        elem = elem.strip()

        if len(elem) > 0:
            short_state = find_val_by_key(elem, states)
            short_cap = find_key_by_val(elem, capital_cities)

            if not short_state and not short_cap:
                print(elem, "is neither a capital city nor a state")
             
            if short_state:
                state = find_key_by_val(short_state, states)
                find_capital(state, short_state, capital_cities)
            elif short_cap:
                capital = find_val_by_key(short_cap, capital_cities)
                find_state(capital, short_cap, states)

def handle_arg():
    states = {
            "Oregon"    : "OR",
            "Alabama"   : "AL",
            "New Jersey": "NJ",
            "Colorado"  : "CO"
    }
    
    capital_cities = {
            "OR": "Salem",
            "AL": "Montgomery",
            "NJ": "Trenton",
            "CO": "Denver"
    }

    if len(sys.argv) != 2:
        exit()

    array = sys.argv[1].split(",")
    parse_array(states, capital_cities, array)

if __name__ == '__main__':
    handle_arg()
