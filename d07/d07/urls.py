"""
The `urlpatterns` list routes URLs to views. For more information please see:
    https://docs.djangoproject.com/en/1.9/topics/http/urls/
Examples:
Function views
    1. Add an import:  from my_app import views
    2. Add a URL to urlpatterns:  url(r'^$', views.home, name='home')
Class-based views
    1. Add an import:  from other_app.views import Home
    2. Add a URL to urlpatterns:  url(r'^$', Home.as_view(), name='home')
Including another URLconf
    1. Add an import:  from blog import urls as blog_urls
    2. Import the include() function: from django.conf.urls import url, include
    3. Add a URL to urlpatterns:  url(r'^blog/', include(blog_urls))
"""
from django.contrib import admin
from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from django.conf.urls import url
from ex01 import views

urlpatterns = [
    url(r'^admin1', admin.site.urls),
    url(r'^login$', auth_views.LoginView.as_view(template_name="ex01/login.html"), name="login"),
    url(r'^$', views.articleView.as_view(), name="home"),
    url(r'detail/(?P<pk>[0-9]+)?', views.detailView.as_view(), name="detail"),
    url(r'^articles$', views.articleView.as_view(), name="articles"),
    url(r'^favourites$', views.favouritesView.as_view(), name="favourites"),
    url(r'^publications$', views.publicationsView.as_view(), name="publications"),
    url(r'^logout$', auth_views.LogoutView.as_view(template_name="ex01/logout.html"), name="logout"),
]
