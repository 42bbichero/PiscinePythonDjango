from django.shortcuts import render, redirect
from django.http import HttpResponse
from django.views.generic.base import TemplateView
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.generic import ListView, FormView, DetailView

from .models import Article
from .models import UserFavoriteArticle

def init(request):
    return redirect(reverse('articles'))

class UserFavoriteArticleView(DetailView):
    model = UserFavoriteArticle

class articleView(ListView):
    model = Article
    context_object_name = 'articles_list'
    template_name = 'ex00/article_view.html'
