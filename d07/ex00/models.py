from django.contrib.auth.models import User
from django.db import models

STATUS_CHOICES = (
        ('d', 'Draft'),
        ('p', 'Published'),
        ('w', 'Withdrawn'),
        )


class Article(models.Model):
    title = models.CharField(max_length=64, null=False)
    author = models.ForeignKey(User, null=False, on_delete=models.DO_NOTHING)
    created = models.DateTimeField(null=False, auto_now=True)
    sinopsis = models.CharField(max_length=312, null=False)
    content = models.TextField(null=False)

    def __str__(self):
        return self.title

class UserFavoriteArticle(models.Model):
    user = models.ForeignKey(User, null=False, on_delete=models.DO_NOTHING)
    article = models.ForeignKey(Article, null=False, on_delete=models.DO_NOTHING)

    def __str__(self):
        return self.article.__str__()
