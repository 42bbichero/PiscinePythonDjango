from django.contrib.auth.models import User
from django.db import models

class Article(models.Model):
    class Meta:
        app_label = 'ex01'

    title = models.CharField(max_length=64, null=False)
    author = models.ForeignKey(User, null=False, on_delete=models.DO_NOTHING, related_name='+')
    created = models.DateTimeField(null=False, auto_now=True)
    sinopsis = models.CharField(max_length=312, null=False)
    content = models.TextField(null=False)

    def __str__(self):
        return self.title

class UserFavouriteArticle(models.Model):
    class Meta:
        app_label = 'ex01'
    user = models.ForeignKey(User, null=False, on_delete=models.DO_NOTHING, related_name='+')
    article = models.ForeignKey(Article, null=False, on_delete=models.DO_NOTHING, related_name='+')

    def __str__(self):
        return self.article.__str__()
