from django.shortcuts import render, redirect
from django.shortcuts import render, redirect
from django.urls import reverse
from django.views.generic import ListView, DetailView
from django.contrib.auth.models import User

from .models import Article, UserFavouriteArticle

def init(request):
    return redirect(reverse('articles'))

class articleView(ListView):
    model = Article
    context_object_name = 'articles_list'
    template_name = 'ex01/article_view.html'

class publicationsView(ListView):
    model = Article
    context_object_name = 'publications_list'
    template_name = 'ex01/publications_view.html'

class favouritesView(ListView):
    model = UserFavouriteArticle
    context_object_name = 'favourites_list'
    template_name = 'ex01/favourites_view.html'

class detailView(DetailView):
    model = Article
    template_name = 'ex01/detail_view.html'
