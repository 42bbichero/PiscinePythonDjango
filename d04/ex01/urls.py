from django.conf.urls import url
from . import views

urlpatterns = [
    url(r'^django/', views.django, name='django'),
    url(r'^template/', views.template, name='template'),
    url(r'^affichage/', views.affichage, name='affichage'),
]
