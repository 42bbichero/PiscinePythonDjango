from django.shortcuts import render

# Create your views here.
def django(request):
        context = {
            'section': {
                'title': 'Ex01 : Django, framework web.'
            },
            'story_list': [
                { 
                    'headline': 'Description',
                    'text': "Django is a high-level Python Web framework that encourages rapid development and clean, pragmatic design."
                },
                {
                    'headline': 'History',
                    'text': 'Django was created in the fall of 2003, when the web programmers at the Lawrence Journal-World newspaper, Adrian Holovaty and Simon Willison, began using Python to build applications. It was released publicly under a BSD license in July 2005. The framework was named after guitarist Django Reinhardt.[18]\nIn June 2008, it was announced that a newly formed Django Software Foundation (DSF) would maintain Django in the future.'
                },
            ]
        }
        return render(request, 'django.html', context)

def template(request):
        context = {
            'section': {
                'title': 'Ex01 : Moteur de template.'
            },
            'story_list': [
                { 
                    'headline': 'Description du moteur de gabarie (template)',
                    'text': 'Ce document présente la syntaxe du langage du système de gabarits de Django. Si vous recherchez une perspective plus technique sur son fonctionnement et sur la manière de l’étendre, consultez Le langage de gabarit de Django : pour les programmeurs Python.\n\nLe langage de gabarit de Django est conçu comme un compromis entre puissance et simplicité. Il est conçu pour que les habitués au code HTML se sentent à l’aise. Si vous avez des connaissances d’autres langages de gabarit orientés texte, tels que Smarty ou Jinja2, vous n’allez pas être dépaysés avec les gabarits de Django.'
                },
                {
                    'headline': 'Definition des blocs',
                    'text': 'Les blocs permettents peuvent etre remplacement dans les template enfant. Pour plus d\'information, se renseginer sur l\'heritage de gabarit.'
                },
                {
                    'headline': 'Les boucles for .. in',
                    'text': 'Avec un tableau en entrer, permettent de boucler sur un ce dernier pour generer toute sorte de code html, ou d\'actions'
                },
                {
                    'headline': 'Les tructure de controle if',
                    'text': 'Tout comme un if normale, les if dans les template peuvent creer des condition dans des template pour appliquer du code si la condition se valide'
                }
            ]
        }
        return render(request, 'template.html', context)

def affichage(request):
        context = {
            'section': {
                'title': 'Ex01 : Processus d’affichage d’une page statique.'
            },
        }
        return render(request, 'ex01/affichage.html', context)
