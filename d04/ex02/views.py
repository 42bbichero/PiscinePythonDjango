from django.http import HttpResponseRedirect
from django.conf import settings
import datetime
from django.shortcuts import render

from .forms import Form

def write_to_file(filename, text):
    with open(filename, 'a') as the_file:
        the_file.write(text + "\n" + str(datetime.datetime.now()) + "\n\n")

def get_from_file(filename):
    try:
        with open(filename, "r") as f:
            template_f = f.readlines()
        f.close()
    except:
        open(filename, 'a').close()
        return []

    return template_f

def init(request):
    context = {}
    context['form'] = Form().as_p()
    context['logs'] = get_from_file(settings.LOG_FILE)

    # if this is a POST request we need to process the form data
    if request.method == 'POST':
        form = Form(request.POST)
        # check whether it's valid:
        text = request.POST.get('text')
        if len(text) > 0:
            context['form'] = form.as_p()
            write_to_file(settings.LOG_FILE, text)
            return HttpResponseRedirect('/ex02')

    return render(request, 'ex02/main.html', context)
