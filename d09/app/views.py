from django.shortcuts import render
from django.contrib.auth import logout
from .forms import LoginForm

# Create your views here.
def login(request):
    url = 'app/main.html'
    context = {}
    context['form'] = LoginForm().as_p()

    return render(request, url, context)

def logout_view(request):
    logout(request)
