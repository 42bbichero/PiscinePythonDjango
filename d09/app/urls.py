from django.conf.urls import url, include
from django.contrib.auth import views as auth_views
from . import views, ajax

urlpatterns = [
    url(r'^account$', auth_views.LoginView.as_view(template_name="app/main.html"), name="login"),
    url(r'^ajax/check_login$', ajax.check_login),
]
