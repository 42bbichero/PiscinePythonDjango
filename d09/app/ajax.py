from django.http import Http404, HttpResponse
from django.contrib.auth.forms import AuthenticationForm
import json
from django.contrib.auth import logout

def logout_view(request):
    logout(request)
    res['result'] = True
    data = json.dumps(res)
    return HttpResponse(data, content_type='application/json')

def check_login(request):
    res = {}
    if request.is_ajax() == True:
        form = AuthenticationForm(data=request.POST)
        if form.is_valid():
            res['login'] = True
            res['username'] = request.POST.get('username')
        else:
            res['login'] = False
            res['form'] = form.as_p()

        data = json.dumps(res)
        return HttpResponse(data, content_type='application/json')
    else:
        raise(Http404)
