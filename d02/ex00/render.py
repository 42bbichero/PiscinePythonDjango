import sys
import os.path
import re

def remove_file(filename):
    if os.path.isfile(filename):
        try:
            os.remove(filename)
        except:
            print("Can't remove", filename)
            exit()

def replace_var(line):
    regex = "(?:\{)(\w+)(?:\})"
    find = re.search(regex, line)
    imported = None

    # Try to import string found
    if find:
        package = "settings"
        name1 = find.group(1)
        try:
            imported = getattr(__import__(package, fromlist=[name1]), name1)
            line = re.sub(regex, imported, line)    
        except AttributeError:
            print("Variable: '{}' isn't placed in settings.py file".format(find.group(1)))
            exit()
        except:
            print("Invalid python syntax in settings.py file")
            exit()

    return line

def parse_file(template):
    try:
        with open(template,"r") as f:
                template_f = f.readlines()
    except:
        print("Can't open given template file.")
        exit()

    file = list()
    for line in template_f:
        if line:
            file.append(replace_var(line))

    try:
        filename = template.split(".")
        new_file = open(filename[0] + ".html","w")
    except:
        print("Can't create new html file.")
        exit()

    for line in file:
        new_file.write(line)

    new_file.close()

def handle_arg(template):
    package = "settings"

    if not os.path.isfile(template):
        print("Given file doesn't exist.")
        exit()

    if not template.endswith(".template"):
        print("Invalid file extension")
        exit()

    remove_file("new_generated_file.html")
    parse_file(template)

if len(sys.argv) != 2:
    print("Invalid argument number.\nUsage: python3 render.py file.template")
    exit()

# Check argument validity
handle_arg(sys.argv[1])
