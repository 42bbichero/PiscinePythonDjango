import random
from beverages import *

class CoffeeMachine:
    def __init__(self):
        self.nb = 0
        self.state = True

    class EmptyCup(HotBeverage):
        price = 0.90
        name = "empty cup"

        def description(self):
            return "An empty cup?! Gimme my money back!"

    class BrokenMachineException(Exception):
        def __init__(self):
            self.message = "This coffee machine has to be repaired."

        def __str__(self):
            return self.message

    def repair(self):
        self.state = True
        self.nb = 0

    def serve(self, beverage: (HotBeverage)):
        ret = random.randint(1, 2)

        if not self.state:
            raise self.BrokenMachineException()

        if self.nb >= 9 and self.state:
            self.state = False

        self.nb+=1

        if ret % 2:
            return self.EmptyCup()
        else:
            return beverage()

if __name__ == '__main__':
    machine = CoffeeMachine()

    ret = machine.serve(HotBeverage)
    print(ret.name)
    ret = machine.serve(Tea)
    print(ret.name)
    ret = machine.serve(Cappucino)
    print(ret.name)
    ret = machine.serve(Coffee)
    print(ret.name)
    ret = machine.serve(Coffee)
    print(ret.name)
    ret = machine.serve(Chocolate)
    print(ret.name)
    ret = machine.serve(Tea)
    print(ret.name)
    ret = machine.serve(HotBeverage)
    print(ret.name)
    ret = machine.serve(HotBeverage)
    print(ret.name)
    ret = machine.serve(Chocolate)
    print(ret.name)

    try:
        print(machine.serve(HotBeverage))
    except Exception as error:
        print("Error catch:", error)
    machine.repair()
    ret = machine.serve(HotBeverage)
    print(ret.name)
    ret = machine.serve(Tea)
    print(ret.name)
    ret = machine.serve(Cappucino)
    print(ret.name)
    ret = machine.serve(Coffee)
    print(ret.name)
    ret = machine.serve(Coffee)
    print(ret.name)
    ret = machine.serve(Chocolate)
    print(ret.name)
    ret = machine.serve(Tea)
    print(ret.name)
    ret = machine.serve(HotBeverage)
    print(ret.name)
    ret = machine.serve(HotBeverage)
    print(ret.name)
    ret = machine.serve(Chocolate)
    print(ret.name)
    try:
        print(machine.serve(HotBeverage))
    except Exception as error:
        print("Error catch:", error)
