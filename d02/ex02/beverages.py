class HotBeverage:
    price = 0.30
    name = "hot beverage"

    def description(self):
        return "Just some hot water in a cup."

    def __str__(self):
        return "price : " + "{0:.2f}".format(round(self.price,2)) + "\n" + "name : " + self.name + "\n" + "description : " + self.description()

class Coffee(HotBeverage):
    price = 0.40
    name = "Coffe"

    def description(self):
        return "A coffee, to stay awake."

class Tea(HotBeverage):
    name = "tea"

    def description(self):
        return "Just some hot water in a cup."

class Chocolate(HotBeverage):
    price = 0.50
    name = "chocolate"

    def description(self):
        return "Chocolate, sweet chocolate..."

class Cappuccino(HotBeverage):
    price = 0.45
    name = "cappuccino"

    def description(self):
        return "Un po’ di Italia nella sua tazza!"

if __name__ == '__main__':
    # Your tests and your error handling
    hot = HotBeverage()
    print(hot)

    coffee = Coffee()
    print(coffee)

    tea = Tea()
    print(tea)

    choco = Chocolate()
    print(choco)

    cappu = Cappuccino()
    print(cappu)
