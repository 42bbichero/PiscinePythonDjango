class Intern:
    def __init__(self, name = "My name? I’m nobody, an intern, I have no name."):
        self.Name = name

    def __str__(self):
        return self.Name

    def make_work(sefl):
        raise Exception("I’m just an intern, I can’t do that...")

    class Coffee:
        def __str__(self):
            return "This is the worst coffee you ever tasted."

    def make_coffee(self):
        return self.Coffee()

if __name__ == '__main__':
    # Your tests and your error handling
    marc = Intern("Marc")
    print(marc)
    no_name = Intern()
    print(no_name)

    coffee = marc.make_coffee()
    print(coffee)

    try:
        no_name.make_work()
    except Exception as error:
        print("Caught this error:", repr(error))
