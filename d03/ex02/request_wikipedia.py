import requests
import sys
import json

def search(search_item):
    S = requests.Session()
    URL = "https://en.wikipedia.org/w/api.php"

    PARAMS = {
            'action': "opensearch",
            'search': search_item,
            'limit': 5,
            'namespace': 0,
            'format': "json"
            }

    R = S.get(url=URL, params=PARAMS)
    data = R.json()

    if not data[1][0]:
        exit("No pages found") 

    PARAMS = {
            "action":"query",
            "prop": "extracts",
            "titles": data[1][0],
            "format":"json",
            "explaintext": "true",
            "prop": "extracts",
            "exintro": "true",
            "exlimit": "max",
            "redirects": "1"
            }

    R = S.get(url=URL, params=PARAMS)
    data = R.json()
    page_id = list(data["query"]["pages"].keys())[0]

    try:
        new_file = open(search_item.replace(" ", "_") + ".wiki","w")
    except:
        print("Can't create wiki file.")
        exit()

    new_file.write(data["query"]["pages"][page_id]["extract"] + '\n')

if __name__ == "__main__":
    if len(sys.argv) != 2:
        exit()

    search(sys.argv[1])
