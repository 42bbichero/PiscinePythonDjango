import sys
import antigravity

def calcul_geohash(latitude, longitude, datenow):
    try:
        antigravity.geohash(float(latitude), float(longitude), datenow.encode())
    except:
        print("Invalid type of latitude or longitude")
        exit()

if __name__ == "__main__":
    if len(sys.argv) != 4:
        print("Error, script must have 3 arguments :\nlatitude : 37.421542\nlongitude:\
 -122.085589\ndate: 2005-05-26-10458.68")
        exit()

    calcul_geohash(sys.argv[1], sys.argv[2], sys.argv[3])
