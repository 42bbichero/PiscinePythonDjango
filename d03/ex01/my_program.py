import sys
sys.path.append("./local_lib/lib/python3.7/site-packages/importlib_metadata")

from path import Path

if __name__ == "__main__":
    d = Path("./test")
    if not d.isdir():
        d.mkdir()

    new = d / "test1.txt"
    if not new.isfile():
        new.touch()
    new.write_text("test2")
    for line in new.lines():
        print (line)
