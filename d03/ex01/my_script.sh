#!/bin/bash

pip_version=$(pip3 --version | cut -d ' ' -f 2)
echo "Pip version: $pip_version"

cur_path=$(pwd)

# check if path exist
if [ ! -d "${cur_path}/local_lib" ]
then
	mkdir local_lib
fi

# I must install path.py hardly codded because when install from git repository, the path.py isn't generated
pip3 install path.py
# pip3 install --log install.log --ignore-installed --install-option="--prefix=${cur_path}/local_lib" git+https://github.com/jaraco/path.py.git 

# execute python program
if [ ! -f "${cur_path}/my_program.py" ]
then
	echo "my_program.py file doesn't exist, abort"
	exit 1
fi

python3 my_program.py
