from django.shortcuts import render
from django.shortcuts import get_object_or_404
from .models import Movies

def populate(request):

    movie = None
    try:
        movie = Movies.objects.get(pk=1)
    except Movies.DoesNotExist:
        movie = None
    else:
        return render(request, 'ex03/main.html', { 'message': "Table isn't present" })

    data = (("1", 'The Phantom Menace', 'George Lucas', 'Rick McCallum', '1999-05-19'),
            ("2", 'Attack of the Clones', 'George Lucas', 'Rick McCallum', '2002-05-16'),
            ("3", 'Revenge of the Sith', 'George Lucas', 'Rick McCallum', '2005-05-19'),
            ("4", 'A New Hope', 'George Lucas', 'Rick McCallum', '1977-05-25'),
            ("5", 'The Empire Strikes Back', 'Irvin Kershner', 'Gary Kruts, Rick McCallum', '1980-05-17'),
            ("6", 'Return of the jedi', 'Richard Marquant', 'Howard G. Kazandjan, George Lucas, Rick McCallum', '1983-05-25'),
            ("7", 'The Force Awakens', 'J.J. Abrams', 'Kathleen Kennedy, J.J. Abram, Bryan Burk', '2015-12-11'))

    message = None
    if not movie:
        for line in data:
            try:
                query = Movies.objects.create(episode_nb=line[0], title=line[1], director=line[2], producer=line[3], release_date=line[4])
            except Exception as e:
                message = line[1] + ":", e
                return render(request, 'ex03/main.html', { 'message': message })
            else:
                query.save()
                if not message:
                    message = "OK\n"
                else:
                    message += "OK\n"
    else:
        message = "Database already populate"

    return render(request, 'ex03/main.html', { 'message': message })

def display(request):
    movies = None
    try:
        movies = Movies.objects.all()
    except:
        return render(request, 'ex03/main.html', { 'message': "No data available" })

    if not movies.exists():
        return render(request, 'ex03/main.html', { 'message': "No data available" })

    return render(request, 'ex03/display.html', { 'movies': movies })
