import psycopg2

class Request:
    def __init__(self, conn, option="one"):
        self.conn = conn
        self.option = option

        try:
            self.curr = conn.cursor()
        except Exception as e:
            raise Exception(e)

    def select(self, req):
        try:
            self.curr.execute(req)
        except Exception as e:
            raise Exception(e)

        res = ''
        if self.option == "one":
            res = self.curr.fetchone()
        elif self.option == "all":
            res = self.curr.fetchall()

        return res

    def insert(self, req):
        try:
            self.curr.execute(req)
        except psycopg2.Error as e:
            self.conn.rollback()
            return e
        except psycopg2.IntegrityError as e:
            self.conn.rollback()
            return e
        else:
            self.conn.commit()

    def close(self):
        self.conn.close()
