from django.db import models
import psycopg2

class Movies(models.Model):
    episode_nb = models.IntegerField()
    opening_crawl = models.TextField()
    title = models.CharField(max_length=64)
    director = models.CharField(max_length=32)
    producer = models.CharField(max_length=128)
    release_date = models.DateField()

    def __str__():
        return self.title
