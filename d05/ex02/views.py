from django.shortcuts import render
from .request import Request
from django.conf import settings
import psycopg2

def display(request):
    conn = psycopg2.connect(
        database=settings.DATABASES['default']['NAME'],
        host=settings.DATABASES['default']['HOST'],
        user=settings.DATABASES['default']['USER'],
        password=settings.DATABASES['default']['PASSWORD']
    )

    result = None
    res_struct = None
    try:
        req = Request(conn, "all")
        res_struct = req.select("select column_name from information_schema.columns where table_schema='public' and table_name='ex02_movies'")
    except: 
        return render(request, 'ex02/main.html', { 'message': "No data available" })

    if not res_struct:
        return render(request, 'ex02/main.html', { 'message': "No data available" })

    try:
        req = Request(conn, "all")
        result = req.select("select * from ex02_movies")
    except: 
        return render(request, 'ex02/main.html', { 'message': "No data available" })

    if not result:
        return render(request, 'ex02/main.html', { 'message': "No data available" })

    req.close()
    return render(request, 'ex02/display.html', { 'struct': res_struct, 'data': result })

def populate(request):
    conn = psycopg2.connect(
        database=settings.DATABASES['default']['NAME'],
        host=settings.DATABASES['default']['HOST'],
        user=settings.DATABASES['default']['USER'],
        password=settings.DATABASES['default']['PASSWORD']
    )

    result = ''
    message = ''
    try:
        req = Request(conn)
        #result = req.select("select column_name from information_schema.columns where table_schema='public' and table_name='ex02_movies'")
        result = req.select("select count(*) from ex02_movies")
    except: 
        return render(request, 'ex02/main.html', { 'message': "table isn't present" })

    data = (("1", 'The Phantom Menace', 'George Lucas', 'Rick McCallum', '1999-05-19'),
                    ("2", 'Attack of the Clones', 'George Lucas', 'Rick McCallum', '2002-05-16'),
                    ("3", 'Revenge of the Sith', 'George Lucas', 'Rick McCallum', '2005-05-19'),
                    ("4", 'A New Hope', 'George Lucas', 'Rick McCallum', '1977-05-25'),
                    ("5", 'The Empire Strikes Back', 'Irvin Kershner', 'Gary Kruts, Rick McCallum', '1980-05-17'),
                    ("6", 'Return of the jedi', 'Richard Marquant', 'Howard G. Kazandjan, George Lucas, Rick McCallum', '1983-05-25'),
                    ("7", 'The Force Awakens', 'J.J. Abrams', 'Kathleen Kennedy, J.J. Abram, Bryan Burk', '2015-12-11'))

    """
        If previous request return a positif number we need to populate the table
    """
    if result[0] == 0:
        for line in data:
            try:
                result = req.insert("INSERT INTO ex02_movies \
                        (episode_nb, title, director, producer, release_date) \
                        VALUES ('%s', '%s', '%s', '%s', '%s');" %
                        (line[0], line[1], line[2], line[3], line[4]))
            except Exception as e: 
                message = line[1] + ":", e
                req.close()
                return render(request, 'ex02/main.html', { 'message': message })
            if result:
                message = line[1], result
                return render(request, 'ex02/main.html', { 'message': message })

            message += "OK\n"
    else:
        message = "Database already populate"
    
    req.close()

    return render(request, 'ex02/main.html', { 'message': message })

def init(request):
    conn = psycopg2.connect(
        database=settings.DATABASES['default']['NAME'],
        host=settings.DATABASES['default']['HOST'],
        user=settings.DATABASES['default']['USER'],
        password=settings.DATABASES['default']['PASSWORD']
    )

    result = ''
    message = ''
    try:
        req = Request(conn)
        result = req.select("select * from information_schema.tables where table_name='ex02_movies'")
    except Exception as e: 
        raise Exception(e)
    else:
        "result: {}".format(result)

    if not result:
        try:
            req.insert("CREATE TABLE ex02_movies (\
                            episode_nb INT PRIMARY KEY,\
                            openging_crawl TEXT,\
                            title varchar(64) UNIQUE,\
                            director varchar(32) NOT NULL,\
                            producer varchar(128) NOT NULL, \
                            release_date date NOT NULL\
                        )")
        except Exception as e: 
            message = e
        else:
            message = "OK"
    else:
        message = "Table already exist"
    req.close()

    return render(request, 'ex02/main.html', { 'message': message })
