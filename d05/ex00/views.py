from django.shortcuts import render
from .request import Request
import psycopg2

def index(request):
    conn = psycopg2.connect(
        database='formationdjango',
        host='localhost',
        user='djangouser',
        password='secret'
    )

    result = ''
    message = ''
    try:
        req = Request(conn)
        result = req.select("select * from information_schema.tables where table_name='ex00_movies'")
    except Exception as e: 
        raise Exception(e)
    else:
        "result: {}".format(result)

    if not result:
        try:
            req.insert("CREATE TABLE ex00_movies (\
                            episode_nb INT PRIMARY KEY,\
                            openging_crawl TEXT,\
                            title varchar(64) UNIQUE NOT NULL,\
                            director varchar(32) NOT NULL,\
                            producer varchar(128) NOT NULL, \
                            release_date date NOT NULL\
                        )")
        except Exception as e: 
            message = e
        else:
            message = "OK"
    else:
        message = "Table already exist"
    req.close()

    return render(request, 'ex00/init.html', { 'message': message })
