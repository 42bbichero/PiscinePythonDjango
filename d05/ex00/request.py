
class Request:
    def __init__(self, conn, option="one"):
        self.conn = conn
        self.option = option

        try:
            self.curr = conn.cursor()
        except Exception as e:
            raise Exception(e[0])

    def select(self, req):
        try:
            self.curr.execute(req)
        except Exception as e:
            raise Exception(e[0])

        res = ''
        if self.option == "one":
            res = self.curr.fetchone()
        elif self.option == "all":
            res = self.curr.fetchall()

        return res

    def insert(self, req):
        try:
            self.curr.execute(req)
        except psycopg2.IntegrityError:
            self.conn.rollback()
            raise Exception("Rollback needed")
        else:
            self.conn.commit()

    def close(self):
        self.conn.close()
