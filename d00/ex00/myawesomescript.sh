#!/bin/sh

# Check argument sent
if [ ${#1} -eq 0 ] || [ ${#2} -ne 0 ]
then
	exit 1;
fi

reg="^(http(s)?:\/\/)?bit\.ly\/"
res=$(echo $1 | grep -E "$reg")

# Check url integrity
if [ ${#res} -eq 0 ]
then
	exit 1;
fi

# Get location moved
location=$(curl -sI $1 | grep "Location: " | cut -d ' ' -f 2)
echo $location
